# Programmieren 1 Tutorium - WS2122

Dies ist das Repository zum Programmieren 1 Tutorium im Wintersemester 2021 / 2022. Hier werden Aufgaben, bestehender Sourcecode und Lösungsvorschläge hochgeladen. Kontaktiert uns gerne für Fragen.
